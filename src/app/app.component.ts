import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  phoneNum;
  form: FormGroup;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      phone: ['']
    });
  }

  alertValue() {
    alert(this.form.value.phone.replace(/[^0-9]/g, ''));
  }
}
