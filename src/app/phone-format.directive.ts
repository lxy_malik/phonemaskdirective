import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appPhoneFormat]'
})
export class PhoneFormatDirective {

  constructor(
    public ngControl: NgControl,
  ) { }

  // for listening every event on host body
  @HostListener('keydown.backspace', ['$event'])
  onBackpress(event) {
    this.onUserAction(event.target.value, true);
  }

  @HostListener('ngModelChange', ['$event'])
  onUserInput(event) {
    this.onUserAction(event, false);
  }

  onUserAction(event, isDel) {
    // Replace everything except numbers
    let maskedValue = event.replace(/[^0-9]/g, '');

    // Delimeter you want in formatting
    const delimeter = ' ';

    // Deletion logic for input field
    if (isDel && maskedValue.length <= 6) {
      maskedValue = maskedValue.substring(0, maskedValue.length);
    }

    // Main masking logic using regex
    if (maskedValue.length === 0) {
      maskedValue = '';
    } else if (maskedValue.length <= 3) {
      maskedValue = maskedValue.replace(/^(\d{0,3})/, `$1`);
    } else if (maskedValue.length <= 6) {
      maskedValue = maskedValue.replace(/^(\d{0,3})(\d{0,3})/, `$1${delimeter}$2`);
    } else if (maskedValue.length <= 10) {
      maskedValue = maskedValue.replace(/^(\d{0,3})(\d{0,3})(\d{0,4})/, `$1${delimeter}$2${delimeter}$3`);
    } else {
      maskedValue = maskedValue.substring(0, 10);
      maskedValue = maskedValue.replace(/^(\d{0,3})(\d{0,3})(\d{0,4})/, `$1${delimeter}$2${delimeter}$3`);
    }
    this.ngControl.valueAccessor.writeValue(maskedValue);
  }

}
